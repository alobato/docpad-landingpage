module.exports = {
  siteMetadata: {
    title: 'DocPad',
  },
  plugins: [
    {
      resolve: `gatsby-source-google-sheet`,
      options: {
        // For protected spreadsheets you can use two-legged OAuth as described here:
        // https://www.npmjs.com/package/google-spreadsheet#service-account-recommended-method
        // creds: {
        //   client_email: `planilhas@sheets-223714.iam.gserviceaccount.com`,
        //   private_key: "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCwVtcCkwL/adQC\nsApKNT6UEP4Xp+F3syd4OKnHpLQx7Wni0pe9dZ4ZuN7SxDQI8NDmvEkNu6S9tbfk\nWnj4rcMGl3M+7XTRRUVv7GtnQuq8DEu65kYnq6YJxVyYXlvIjb7sCSy063d5HejJ\n+c2BEzSl9DhsK7KCwSjyJMWkFrTSQZLB2RuBPea0pNaye+gbDV7QEEl4H4SIaDO3\nN031dqsyyd7JtWRpuWh0pb/17lqCphxg6wIiAcCfUkZuAgHUeiEyO1U6vEuIEeMZ\nrEauQ9jPuam9lDnyEaM/CH5wQgs90U7PWDHc0yU7C2RoSBlQORryKnRiucbBHm3Q\nKEvWdYFDAgMBAAECggEARenQKkmeHSTYtU4VblGI44yPKVLavioOIoiYq7o95TOj\nURY8qhWZXspgscCvfsH3mRuafTLhwINtCsDiCHpgAduHCjJ8J8JmotbhyFmMFo1r\ncwj8VJsl4+bXbOOwUSX9xbhnfL2Eq0Tz+Cwt0ZiLPabDvgNfLZ8mvEZfA80q2NcV\nNP1JnykIgoWYYofFDePLDBUgk6O7ZE9AYLdHbQDA5w0G7YpER/zHIp76Xa1QcWdH\nX2vWpQGy1OM5rBVjvpxC06H44G5pdVBaohnGRwR27jbFh1iYHlguEYupCjXDb73V\nS2tjQHXAb/7+PNfISht/3eVnyD3fAptt2g/ErXu5sQKBgQDYyac1rnwQbtHn9CNF\nUyBtfk/Kg5c+2qi43aVaFEWJ25GDVliJMvturTk1cqaggrVv6g6SL/fONzzsiKNd\n763pnjI32cSgOqykW+RF1l6gXZ4OJsdKK7RzdwEMf0eMzs+gxk0qocKm9C/4MJSF\n+4ROXNQjlr0ZgKsMmvj+KCxq0wKBgQDQPDiNTMcXciTbNHmdr4mU7fwYswpS2diO\nrZywvNb2QhATbIh4QPagNjaTG4XQs9aEi5krWYAiDpBaWd+wUA0L6xpx7t5rJCe4\n5Zc/9v8/Arbsc5d815uSrDQhPJKxQdhsKiFuUWUWSD34GAQUBit8hhK3bEd7zYDW\nqIugw3+p0QKBgHaoETSJt3BLJ+scrr2AHUyxCT1gP0Vw606cSqf+Vn15R7EqbrVR\nyVsvxxNhGKDcSjDiXG1ZgfpGM7uHoluFF5y6MORu9nd5BgvEHDK5ffuqohs+0G/h\nchiM927+r4v7mpt2TsBZ37AAEc130aNwIl+94YNFO2WdDoKMxGJgXMdvAoGAag3h\nt4Vc+MH3p3ToY7kdVy6F8IZvQZcVxkUBwrnKuY0X5uyl7KkZcqtpzC5+TrbpEISQ\ntBVXPwEv3/RaCtAadPF+fHxt5x9446B7EebYRN07EN4mTzz4TRFLU4ooJAMnGJ+0\nex0VYO3WMd75ZMFnsHoQWj7ZgWI+thUNWAl5oaECgYAr5Fn8iTIug4aA9LFrD2Om\nTrPB07iaIAaFU7G3mQOF0h1KkI3MDDUYU8MWT53btcfp8glwUiTSw6QkI8CYqyW+\nAl3cNGbmp1cJk9lCqEcxHSJg7N3/qHGlf6P91KjKPKCTwWY4WleFB7vLtRyITlxh\nyCEFmgF+dJAsFxEqQGhhYA==\n-----END PRIVATE KEY-----\n"
        // },
        // This is the bit after "/d/" and before "/edit" in the URL of a
        // Google Sheets document. I.e.,
        // https://docs.google.com/spreadsheets/d/1ec1bO25bbEL4pdZjhlV3AppMtnO65D0ZI8fXy4z47Dw/edit#gid=0
        spreadsheetKey: `1Hq4NheCO1j3IXa7D0odTfuI6hGzf9e8l6OtovjfEN4U`,
        // rootName: 'RootName' // default is Sheet
      }
    }
  ],
}

import styled from 'styled-components'

const Avatar = styled.div`  
  width: 180px; height: 180px;
  border-radius: 50%;
  padding: 14px;
  display: flex;
  justify-content: center;
  align-items: center;
  @media (min-width: 576px) {
    width: 180px; height: 180px;
  }
  @media (min-width: 768px) {
    width: 180px; height: 180px;
  }
  @media (min-width: 992px) {
    width: 180px; height: 180px;
  }
  @media (min-width: 1200px) {
    width: 180px; height: 180px;
  }
  & > .bg {
    background-image: url(${props => props.image});
    background-size: cover;
    width: 180px;
    height: 180px;
    background-color: green;
    background-size: contain;
    border-radius: 50%;
    position: absolute;
    transition: 0.5s;
  }
  & > .caption {
    opacity: 0;
    z-index: 1;
    transition: 0.5s;
    text-align: center;
  }
  &:hover {
    & > .caption {
      opacity: 1;
    }
    & > .bg {
      opacity: 0.3;
    }
  }
  a {
    text-decoration: none;
    color: black;
  }
`

export default Avatar

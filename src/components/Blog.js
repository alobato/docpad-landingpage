import styled from 'styled-components'

const Blog = styled.div`
  display: flex;
  margin: 0 -10px;
  flex-direction: column;
  @media (min-width: 576px) {
    flex-direction: column;
  }
  @media (min-width: 768px) {
    flex-direction: row;
  }
  @media (min-width: 992px) {
    flex-direction: row;
  }
  @media (min-width: 1200px) {
    flex-direction: row;
  } 
`

export default Blog

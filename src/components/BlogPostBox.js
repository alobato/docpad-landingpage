import styled from 'styled-components'
import FamilyImage from '../images/placeholder.png'

const BlogPostBox = styled.div`
  background-image: url(${FamilyImage});
  background-size: cover;
  width: 100%;
  overflow: hidden;
  flex-grow: 1;
  margin: 10px;
  height: 300px;
  
  & > .overlay {
    background-color: rgba(0, 0, 0, 0.5);
    transition: 0.3s;
    height: 300px;
    transform: translateY(80%);
    & > .headline {
      display: block;
      color: white;
      padding: 20px;
    }
    & > .button {
      display: block;
      color: white;
    }
  }
  &:hover {
    & > .overlay {
      transform: translateY(0);
    }
  }
  a {
    text-decoration: none;
  }
`

export default BlogPostBox

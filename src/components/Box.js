import styled from 'styled-components'

const Box = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  width: 80%;
  margin: 0px auto;
  & :nth-child(1) { order: 2; }
  /* & :nth-child(2) { text-align: center; } */
  @media (min-width: 768px) {
    flex-direction: row;
    & :nth-child(1) { order: 0; }
  }  
  
`

export default Box

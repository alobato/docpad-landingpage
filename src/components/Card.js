import styled from 'styled-components'
import mbi2 from '../images/mbi2.png'
import mci2 from '../images/mci2.png'
import mc2 from '../images/mc2.png'

const Card = styled.div`
  margin: 20px;
  margin-bottom: 30px;
  background-color: white;
  padding: 20px;
  @media (min-width: 576px) {
    padding: 30px;
  }
  @media (min-width: 768px) {
    padding: 40px;
  }

  ${({ how }) => how && `
    @media (min-width: 1200px) {
      background: url(${mbi2}) left top no-repeat, url(${mci2}) right top no-repeat;
      background-size: 250px, 250px;
      background-color: white;
    }
  `}

  ${({ who }) => who && `
    @media (min-width: 1200px) {
      background: url(${mci2}) left top no-repeat, url(${mc2}) right bottom no-repeat;
      background-size: 450px, 450px;
      background-color: white;
    }
  `}

  ${({ clients }) => clients && `
    @media (min-width: 1200px) {
      background: url(${mbi2}) left top no-repeat, url(${mci2}) right top no-repeat;
      background-size: 150px, 150px;
      background-color: white;
    }
  `}
`

export default Card

import React from 'react'
import styled from 'styled-components'

const Content = ({ className, children }) => (
  <div className={className}>
    {children}
  </div>
)

const StyledContent = styled(Content)`
  background-color: #F3F3F3;
  padding: 10px;
  @media (min-width: 576px) {
    padding: 20px;
  }
  @media (min-width: 768px) {
    padding: 30px;
  }
`

export default StyledContent

import React from 'react'
import styled from 'styled-components'

const Icon = styled.div`
  text-align: center;
  border-radius: 50%!important;
  border: solid 1px #A5A5A5;
  color: inherit;
  width: 60px;
  height: 60px;
  display: flex;
  align-items: center;
  justify-content: center;
  ${({ active }) => active && `
    background-color: #E1E2E7;
  `}
`

const Avatar = styled.div`
  display: flex;
  align-items: center;
  margin: 0 15px;
`

const Title = styled.div`
  font-size: 18px;
  color: inherit;
  text-transform: uppercase;
  ${({ reverse }) => reverse && `
    text-align: right;
  `}
`

const Subtitle = styled.div`
  font-size: 14px;
  color: inherit;
  ${({ reverse }) => reverse && `
    text-align: right;
  `}
`

const FeatureCard = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  align-items: center;
  width: auto;
  height: 80px;
  ${({ reverse }) => reverse && `
    flex-direction: row-reverse;
  `}
  margin: 20px 0;
  color: #A5A5A5;
  cursor: pointer;
`

const DescriptionCard = ({ title, subtitle, icon, reverse = false, active = false, onClick }) => (
  <FeatureCard reverse={reverse} onClick={onClick}>
    <Avatar>
      <Icon active={active}>
        {icon}
      </Icon>
    </Avatar>
    <div style={{alignItems: 'center'}}>
      <Title reverse={reverse}>{title}</Title>
      <Subtitle reverse={reverse}>{subtitle}</Subtitle>
    </div>
  </FeatureCard>
)

export default DescriptionCard

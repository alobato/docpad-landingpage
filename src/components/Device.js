import styled from 'styled-components'

const Device = styled.div`
  max-width: 450px;
`

export default Device

import React, { Component } from 'react'
import styled from 'styled-components'

class Feature extends Component {

  render() {
    const { title, subtitle, icon, border } = this.props

    const Wrapper = styled.div`
      border-right: none;
      padding: 30px 30px;
      @media (min-width: 576px) {
        border-right: none;
      }
      @media (min-width: 768px) {
        border-right: ${border};
        padding: 30px 60px;
      }
      @media (min-width: 992px) {
        border-right: ${border};
        padding: 30px 80px;
      }
      @media (min-width: 1200px) {
        border-right: ${border};
        padding: 30px 120px;
      }
    `

    const Icon = styled.div`
      text-align: center;
      color: #A5A5A5;
    `

    const Title = styled.div`
      font-size: 50px;
      text-align: center;
      font-size: 28px;
      color: #70ACDA;
      font-weight: 500;
      margin: 10px;
    `

    const Subtitle = styled.div`
      font-size: 16px;
      text-align: center;
      color: #A5A5A5;
    `

    return (
      <Wrapper>
        <Icon>{icon}</Icon>
        <Title>{title}</Title>
        <Subtitle>{subtitle}</Subtitle>
      </Wrapper>
    )
  }

}

export default Feature

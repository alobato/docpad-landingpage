import React from 'react'
import styled from 'styled-components'

const facebookIcon = (<svg height='30' fill='currentColor' viewBox='0 0 50 50'><path d='M25,2C12.318,2,2,12.317,2,25s10.318,23,23,23c12.683,0,23-10.317,23-23S37.683,2,25,2z M32,16h-3.29 C26.772,16,26,16.455,26,17.806V21h6l-1,5h-5v13h-6V26h-3v-5h3v-2.774C20,14.001,21.686,11,26.581,11C29.203,11,32,12,32,12V16z'></path></svg>)
const linkedinIcon = (<svg height='30' fill='currentColor' viewBox='0 0 50 50'><path d='M25,2C12.318,2,2,12.317,2,25s10.318,23,23,23s23-10.317,23-23S37.682,2,25,2z M18,35h-4V20h4V35z M16,17 c-1.105,0-2-0.895-2-2c0-1.105,0.895-2,2-2s2,0.895,2,2C18,16.105,17.105,17,16,17z M37,35h-4v-5v-2.5c0-1.925-1.575-3.5-3.5-3.5 S26,25.575,26,27.5V35h-4V20h4v1.816C27.168,20.694,28.752,20,30.5,20c3.59,0,6.5,2.91,6.5,6.5V35z'></path></svg>)
const chevronUp = (<svg height='30' fill='currentColor' viewBox='0 0 40 40'><g><path d='M 20 21.304688 L 3.457031 33.300781 L 1.699219 30.894531 L 20 17.621094 L 38.300781 30.894531 L 36.542969 33.300781 Z' /><path d='M 20 10.304688 L 3.457031 22.308594 L 1.699219 19.894531 L 20 6.617188 L 38.300781 19.894531 L 36.542969 22.308594 Z' /></g></svg>)

const Footer = ({ className }) => (
  <div className={className}>
    <div style={{lineHeight: '30px'}}>© 2016 - 2018 - DOCPAD</div>
    <div style={{display: 'flex', color: 'white', cursor: 'pointer'}} onClick={() => window.scrollTo(0, 0)}>{chevronUp}</div>
    <div style={{display: 'flex', height: 30}}>
      <div style={{marginRight: 10, lineHeight: 0}}><img style={{height: 30}} src='https://cdn.worldvectorlogo.com/logos/google-play-download-android-app.svg' alt='Google Play' /></div>
      <div style={{marginRight: 10, lineHeight: 0}}><img style={{height: 30}} src='https://cdn.worldvectorlogo.com/logos/download-on-the-app-store-apple.svg' alt='App Store' /></div>
      <div style={{color: 'white', marginRight: 10, lineHeight: 0}}>{facebookIcon}</div>
      <div style={{color: 'white', lineHeight: 0}}>{linkedinIcon}</div>
    </div>
  </div>
)

const StyledFooter = styled(Footer)`
  display: flex;
  justify-content: space-between;
  color: white;
  background: linear-gradient(90deg, rgba(197,222,239,1) 0%, rgba(197,222,239,1) 20%, rgba(224,177,189,1) 80%, rgba(224,177,189,1) 100%);
  padding: 30px;
`

export default StyledFooter

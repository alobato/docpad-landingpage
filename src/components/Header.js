import React from 'react'
import styled from 'styled-components'
// import FamilyImage from '../images/family.jpg'
import FamilyImage from '../images/headerbg.jpg'
import LogoImage from '../images/logo.png'

const Headline = styled.h1`
  position: absolute;
  bottom: 30px;
  left: 30px;
  color: white;
  font-size: inherit;
  font-weight: 600;
  width: 600px;
`

const Top = styled.div`
  width: 100%;
  display: block;
  font-size: 15px;
  @media (min-width: 992px) {
    display: flex;
    justify-content: space-between;
    padding: 15px;
    font-size: 16px;
  }
`

const Nav = styled.div`
  max-width: 100%;
  height: 44px;
  overflow: hidden;
  @media (min-width: 992px) {
    display: block;
    width: auto;
  }
`

const NavInner = styled.div`
display: flex;
align-items: center;
padding-bottom: 2rem;
overflow-x: auto;
white-space: nowrap;
`

const Logo = styled.div`
  text-align: center;
  padding: 20px;
  @media (min-width: 992px) {
    text-align: left;
    width: auto;
  }
`

const usIcon = (
<svg width='30' viewBox="0 0 48 48">
<g>
<path style={{fill: '#ECEFF1'}} d="M 1.996094 10 L 45.996094 10 L 45.996094 37 L 1.996094 37 Z "></path>
<path style={{fill: '#F44336'}} d="M 2 10 L 46 10 L 46 13 L 2 13 Z "></path>
<path style={{fill: '#F44336'}} d="M 2 16 L 46 16 L 46 19 L 2 19 Z "></path>
<path style={{fill: '#F44336'}} d="M 2 22 L 46 22 L 46 25 L 2 25 Z "></path>
<path style={{fill: '#F44336'}} d="M 2 28 L 46 28 L 46 31 L 2 31 Z "></path>
<path style={{fill: '#F44336'}} d="M 2 34 L 46 34 L 46 37 L 2 37 Z "></path>
<path style={{fill: '#3F51B5'}} d="M 2 10 L 23 10 L 23 25 L 2 25 Z "></path>
<path style={{fill: '#FFFFFF'}} d="M 4.25 12 L 4.714844 12.988281 L 5.75 13.144531 L 5 13.914063 L 5.179688 15 L 4.25 14.488281 L 3.320313 15 L 3.5 13.914063 L 2.75 13.144531 L 3.785156 12.988281 Z "></path>
<path style={{fill: '#FFFFFF'}} d="M 8.25 12 L 8.714844 12.988281 L 9.75 13.144531 L 9 13.914063 L 9.179688 15 L 8.25 14.488281 L 7.320313 15 L 7.5 13.914063 L 6.75 13.144531 L 7.785156 12.988281 Z "></path>
<path style={{fill: '#FFFFFF'}} d="M 12.25 12 L 12.714844 12.988281 L 13.75 13.144531 L 13 13.914063 L 13.179688 15 L 12.25 14.488281 L 11.320313 15 L 11.5 13.914063 L 10.75 13.144531 L 11.785156 12.988281 Z "></path>
<path style={{fill: '#FFFFFF'}} d="M 16.25 12 L 16.714844 12.988281 L 17.75 13.144531 L 17 13.914063 L 17.179688 15 L 16.25 14.488281 L 15.320313 15 L 15.5 13.914063 L 14.75 13.144531 L 15.785156 12.988281 Z "></path>
<path style={{fill: '#FFFFFF'}} d="M 20 12 L 20.464844 12.988281 L 21.5 13.144531 L 20.75 13.914063 L 20.929688 15 L 20 14.488281 L 19.070313 15 L 19.25 13.914063 L 18.5 13.144531 L 19.535156 12.988281 Z "></path>
<path style={{fill: '#FFFFFF'}} d="M 4.25 20 L 4.714844 20.988281 L 5.75 21.144531 L 5 21.914063 L 5.179688 23 L 4.25 22.488281 L 3.320313 23 L 3.5 21.914063 L 2.75 21.144531 L 3.785156 20.988281 Z "></path>
<path style={{fill: '#FFFFFF'}} d="M 8.25 20 L 8.714844 20.988281 L 9.75 21.144531 L 9 21.914063 L 9.179688 23 L 8.25 22.488281 L 7.320313 23 L 7.5 21.914063 L 6.75 21.144531 L 7.785156 20.988281 Z "></path>
<path style={{fill: '#FFFFFF'}} d="M 12.25 20 L 12.714844 20.988281 L 13.75 21.144531 L 13 21.914063 L 13.179688 23 L 12.25 22.488281 L 11.320313 23 L 11.5 21.914063 L 10.75 21.144531 L 11.785156 20.988281 Z "></path>
<path style={{fill: '#FFFFFF'}} d="M 16.25 20 L 16.714844 20.988281 L 17.75 21.144531 L 17 21.914063 L 17.179688 23 L 16.25 22.488281 L 15.320313 23 L 15.5 21.914063 L 14.75 21.144531 L 15.785156 20.988281 Z "></path>
<path style={{fill: '#FFFFFF'}} d="M 20 20 L 20.464844 20.988281 L 21.5 21.144531 L 20.75 21.914063 L 20.929688 23 L 20 22.488281 L 19.070313 23 L 19.25 21.914063 L 18.5 21.144531 L 19.535156 20.988281 Z "></path>
<path style={{fill: '#FFFFFF'}} d="M 5.25 16 L 5.714844 16.988281 L 6.75 17.144531 L 6 17.914063 L 6.179688 19 L 5.25 18.488281 L 4.320313 19 L 4.5 17.914063 L 3.75 17.144531 L 4.785156 16.988281 Z "></path>
<path style={{fill: '#FFFFFF'}} d="M 9.25 16 L 9.714844 16.988281 L 10.75 17.144531 L 10 17.914063 L 10.179688 19 L 9.25 18.488281 L 8.320313 19 L 8.5 17.914063 L 7.75 17.144531 L 8.785156 16.988281 Z "></path>
<path style={{fill: '#FFFFFF'}} d="M 13.25 16 L 13.714844 16.988281 L 14.75 17.144531 L 14 17.914063 L 14.179688 19 L 13.25 18.488281 L 12.320313 19 L 12.5 17.914063 L 11.75 17.144531 L 12.785156 16.988281 Z "></path>
<path style={{fill: '#FFFFFF'}} d="M 17.25 16 L 17.714844 16.988281 L 18.75 17.144531 L 18 17.914063 L 18.179688 19 L 17.25 18.488281 L 16.320313 19 L 16.5 17.914063 L 15.75 17.144531 L 16.785156 16.988281 Z "></path>
<path style={{fill: '#FFFFFF'}} d="M 21 16 L 21.464844 16.988281 L 22.5 17.144531 L 21.75 17.914063 L 21.929688 19 L 21 18.488281 L 20.070313 19 L 20.25 17.914063 L 19.5 17.144531 L 20.535156 16.988281 Z "></path>
</g>
</svg>
)

const brIcon = (
<svg width='30' viewBox="0 0 48 48">
  <g>
    <path style={{fill: '#4CAF50'}} d="M 2 9 L 46 9 L 46 39 L 2 39 Z "></path>
    <path style={{fill: '#FFEB3B'}} d="M 42 24 L 24 36 L 6 24 L 24 12 Z "></path>
    <path style={{fill: '#3F51B5'}} d="M 31 24 C 31 27.867188 27.867188 31 24 31 C 20.132813 31 17 27.867188 17 24 C 17 20.132813 20.132813 17 24 17 C 27.867188 17 31 20.132813 31 24 Z "></path>
    <path style={{fill: '#FFFFFF'}} d="M 17.804688 20.746094 C 17.464844 21.390625 17.230469 22.09375 17.105469 22.835938 C 19.359375 22.558594 25.484375 22.273438 30.640625 26.210938 C 30.871094 25.515625 31 24.773438 31 24 C 31 24 31 23.996094 31 23.992188 C 26.058594 20.644531 20.65625 20.511719 17.804688 20.746094 Z "></path>
  </g>
</svg>
)


const searchIcon = (<svg fill='currentColor' width='20' viewBox='0 0 50 50'><path d='M 21 3 C 11.601563 3 4 10.601563 4 20 C 4 29.398438 11.601563 37 21 37 C 24.355469 37 27.460938 36.015625 30.09375 34.34375 L 42.375 46.625 L 46.625 42.375 L 34.5 30.28125 C 36.679688 27.421875 38 23.878906 38 20 C 38 10.601563 30.398438 3 21 3 Z M 21 7 C 28.199219 7 34 12.800781 34 20 C 34 27.199219 28.199219 33 21 33 C 13.800781 33 8 27.199219 8 20 C 8 12.800781 13.800781 7 21 7 Z' /></svg>)

const navs = {
  howItWorks: {en: 'How It Works', pt: 'Como Funciona'},
  plans: {en: 'Plans', pt: 'Planos'},
  whoWeAre: {en: 'Who We Are', pt: 'Quem Somos'},
  contact: {en: 'Contact', pt: 'Contato'}
}

const Header = ({ className, headline, lang }) => (
  <div className={className}>
    <Top>
      <Logo>
        <img src={LogoImage} style={{width: '100px'}} />
      </Logo>
      <Nav className='nav'>
        <NavInner>
          <a href='/' className='active'>Home</a>
          <a href='#como-funciona'>{navs.howItWorks[lang]}</a>
          <a href='#planos'>{navs.plans[lang]}</a>
          <a href='#quem-somos'>{navs.whoWeAre[lang]}</a>
          <a href='#blog'>Blog</a>
          <a href='#contato'>{navs.contact[lang]}</a>
          <a href='/' style={{
            border: '1px solid white',
            color: 'white',
            borderRadius: 20,
            padding: '9px 30px'
          }}>Login</a>

          <a href={lang === 'en' ? '/' : '/en'} style={{lineHeight: 0}}>
            {lang === 'en' ? brIcon :  usIcon} 
          </a>

        </NavInner>



        <span style={{color: 'white'}}>{searchIcon}</span>
      </Nav>
    </Top>
    <Headline dangerouslySetInnerHTML={{ __html: headline }}></Headline>
  </div>
)

const StyledHeader = styled(Header)`
  background-image: url(${FamilyImage});
  background-size: cover;
  position: relative;
  background-repeat: no-repeat;
  background-position: center;
  height: 300px;
  font-size: 34px;
  @media (min-width: 576px) {
    height: 400px;
    font-size: 48px;
  }
  @media (min-width: 768px) {
    height: 500px;
    font-size: 52px;
  }
  @media (min-width: 992px) {
    height: 600px;
    font-size: 64px;
  }
  @media (min-width: 1200px) {
    height: 700px;
    font-size: 72px;
  }
`

export default StyledHeader

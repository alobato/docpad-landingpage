import styled from 'styled-components'

const LinkButton = styled.a`
  display: inline-block;
  color: #BF4251;
  border: 2px solid #BF4251;
  padding: 15px 60px  ;
  border-radius: 30px;
  text-align: center;
  font-weight: 500;
`

export default LinkButton

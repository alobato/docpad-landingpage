import styled from 'styled-components'

const TwoColumnsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  justify-content: center;
  align-items: center;
  @media (min-width: 576px) {
    flex-direction: column;
  }
  @media (min-width: 768px) {
    flex-direction: row;
  }
  @media (min-width: 992px) {
    flex-direction: row;
  }
  @media (min-width: 1200px) {
    flex-direction: row;
  }   
`

export default TwoColumnsWrapper

import React from 'react'
import { graphql, withPrefix } from 'gatsby'  
import Slider from 'react-slick'

import PatientImage from '../../images/patients.jpg'
import DoctorImage from '../../images/doctors.jpg'
import InstitutionImage from '../../images/institutions.jpg'

import ClientsImage from '../../images/clients.png'

import PlaceholderImage from '../../images/placeholder.png'
import CelImage from '../../images/cel.png'
import TabletImage from '../../images/tablet.jpg'
import NoteImage from '../../images/note.jpg'

import Layout from '../../components/Layout'
import FeatureWrapper from '../../components/FeatureWrapper'
import Feature from '../../components/Feature'
import Card from '../../components/Card'
import DescriptionCard from '../../components/DescriptionCard'
import LinkButton from '../../components/LinkButton'
import Box from '../../components/Box'
import Device from '../../components/Device'
import TwoColumnsWrapper from '../../components/TwoColumnsWrapper'
import Avatar from '../../components/Avatar'
import BlogPostBox from '../../components/BlogPostBox'
import Blog from '../../components/Blog'

import Andre from '../../images/avatar_andre.jpg'
import Antonio from '../../images/avatar_antonio.jpg'
import Bruno from '../../images/avatar_bruno.jpg'


const leftIcon = (
  <svg width='30' viewBox="0 0 49.646 49.646">
  <g transform="translate(17.541 15.071)">
    <circle fill="#3fafe4" cx="24" cy="24" r="24" transform="translate(30.937 34) rotate(180)"/>
    <rect fill="#fff" width="3.297" height="15.47" rx="1.648" transform="matrix(0.643, -0.766, 0.766, 0.643, 0, 10.189)"/>
    <rect fill="#fff" width="3.297" height="15.47" rx="1.648" transform="translate(2.143 12.298) rotate(-129)"/>
  </g>
</svg>
)

const rightIcon = (
<svg width='30' viewBox="0 0 48 48">
  <g transform="translate(30.938 34) rotate(180)">
    <path fill="#3fafe4" d="M24,0A24,24,0,1,1,0,24,24,24,0,0,1,24,0Z" transform="translate(30.938 34) rotate(-180)"/>
    <rect fill="#fff" width="3.297" height="15.47" rx="1.648" transform="matrix(0.643, -0.766, 0.766, 0.643, 0, 10.189)"/>
    <rect fill="#fff" width="3.297" height="15.47" rx="1.648" transform="translate(2.143 12.298) rotate(-129)"/>
  </g>
</svg>
)

const linkedinIcon = (<svg fill='#0288D1' width='24' viewBox='0 0 24 24'><path d='M19,3H5C3.895,3,3,3.895,3,5v14c0,1.105,0.895,2,2,2h14c1.105,0,2-0.895,2-2V5C21,3.895,20.105,3,19,3z M9,17H6.477v-7H9 V17z M7.694,8.717c-0.771,0-1.286-0.514-1.286-1.2s0.514-1.2,1.371-1.2c0.771,0,1.286,0.514,1.286,1.2S8.551,8.717,7.694,8.717z M18,17h-2.442v-3.826c0-1.058-0.651-1.302-0.895-1.302s-1.058,0.163-1.058,1.302c0,0.163,0,3.826,0,3.826h-2.523v-7h2.523v0.977 C13.93,10.407,14.581,10,15.802,10C17.023,10,18,10.977,18,13.174V17z' /></svg>)

const calendarIcon = (<svg fill='currentColor' width='30' viewBox='0 0 26 26'><path d='M 7 0 C 6.449219 0 6 0.449219 6 1 L 6 4 C 6 4.550781 6.449219 5 7 5 C 7.550781 5 8 4.550781 8 4 L 8 1 C 8 0.449219 7.550781 0 7 0 Z M 19 0 C 18.449219 0 18 0.449219 18 1 L 18 4 C 18 4.550781 18.449219 5 19 5 C 19.550781 5 20 4.550781 20 4 L 20 1 C 20 0.449219 19.550781 0 19 0 Z M 3 2 C 1.34375 2 0 3.34375 0 5 L 0 23 C 0 24.65625 1.34375 26 3 26 L 23 26 C 24.65625 26 26 24.65625 26 23 L 26 5 C 26 3.34375 24.65625 2 23 2 L 21 2 L 21 4 C 21 5.105469 20.105469 6 19 6 C 17.894531 6 17 5.105469 17 4 L 17 2 L 9 2 L 9 4 C 9 5.105469 8.105469 6 7 6 C 5.894531 6 5 5.105469 5 4 L 5 2 Z M 2 9 L 24 9 L 24 23 C 24 23.550781 23.550781 24 23 24 L 3 24 C 2.449219 24 2 23.550781 2 23 Z M 9 12 L 9 14.3125 L 13.8125 14.3125 L 10.03125 21.96875 L 13.5 21.96875 L 16.96875 13.53125 L 16.96875 12 Z' /></svg>)
const plusIcon = (<svg fill='currentColor' width='30' viewBox='0 0 26 26'><path d='M 12 3 C 11.398438 3 11 3.398438 11 4 L 11 11 L 4 11 C 3.398438 11 3 11.398438 3 12 L 3 14 C 3 14.601563 3.398438 15 4 15 L 11 15 L 11 22 C 11 22.601563 11.398438 23 12 23 L 14 23 C 14.601563 23 15 22.601563 15 22 L 15 15 L 22 15 C 22.601563 15 23 14.601563 23 14 L 23 12 C 23 11.398438 22.601563 11 22 11 L 15 11 L 15 4 C 15 3.398438 14.601563 3 14 3 Z' /></svg>)
const avatarIcon = (<svg fill='currentColor' width='30' height='30' viewBox='0 0 16 16'><path d='M 3 1 L 3 2 L 2.5 2 C 1.675781 2 1 2.675781 1 3.5 L 1 12.5 C 1 13.324219 1.675781 14 2.5 14 L 12.5 14 C 13.324219 14 14 13.324219 14 12.5 L 14 3.5 C 14 2.675781 13.324219 2 12.5 2 L 12 2 L 12 1 L 11 1 L 11 2 L 4 2 L 4 1 Z M 2.5 3 L 3 3 L 3 4 L 4 4 L 4 3 L 11 3 L 11 4 L 12 4 L 12 3 L 12.5 3 C 12.78125 3 13 3.21875 13 3.5 L 13 5 L 2 5 L 2 3.5 C 2 3.21875 2.21875 3 2.5 3 Z M 2 6 L 13 6 L 13 12.5 C 13 12.78125 12.78125 13 12.5 13 L 2.5 13 C 2.21875 13 2 12.78125 2 12.5 Z M 5 7 L 5 8 L 6 8 L 6 7 Z M 7 7 L 7 8 L 8 8 L 8 7 Z M 9 7 L 9 8 L 10 8 L 10 7 Z M 11 7 L 11 8 L 12 8 L 12 7 Z M 3 9 L 3 10 L 4 10 L 4 9 Z M 5 9 L 5 10 L 6 10 L 6 9 Z M 7 9 L 7 10 L 8 10 L 8 9 Z M 9 9 L 9 10 L 10 10 L 10 9 Z M 11 9 L 11 10 L 12 10 L 12 9 Z M 3 11 L 3 12 L 4 12 L 4 11 Z M 5 11 L 5 12 L 6 12 L 6 11 Z M 7 11 L 7 12 L 8 12 L 8 11 Z'></path></svg>)
const pinIcon = (<svg fill='currentColor' width='16' viewBox='0 0 48 48'><path d='M24,1C15.2,1,6.015,7.988,6,18C5.982,29.981,24,48,24,48s18.019-17.994,18-30   C41.984,8.003,32.8,1,24,1z M24,26c-4.418,0-8-3.582-8-8s3.582-8,8-8s8,3.582,8,8S28.418,26,24,26z' /></svg>)

const sliderSettings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  arrows: true,
}

class IndexPage extends React.Component {
  constructor(props) {
    super(props);
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.nextDevice = this.nextDevice.bind(this);
    this.previousDevice = this.previousDevice.bind(this);
    this.state = {currentDevice: 'Paciente'}
  }

  componentDidMount() {
    // TODO: Salvar preferências do usuário (lang) no localstorage
    // const lang = navigator.language || navigator.userLanguage
    // if (lang.includes('en'))
    //   document.location.href = 'http://localhost:8000/br'
  }

  next() {
    this.slider.slickNext();
  }
  previous() {
    this.slider.slickPrev();
  }

  nextDevice() {
    if (this.state.currentDevice === 'Paciente') {
      this.setState({currentDevice: 'Médico'}, () => this.devicesSlider.slickGoTo(1))
    }
    if (this.state.currentDevice === 'Médico') {
      this.setState({currentDevice: 'Instituição'}, () => this.devicesSlider.slickGoTo(2))
    }
    if (this.state.currentDevice === 'Instituição') {
      this.setState({currentDevice: 'Paciente'}, () => this.devicesSlider.slickGoTo(0))
    }
  }
  previousDevice() {
    if (this.state.currentDevice === 'Paciente') {
      this.setState({currentDevice: 'Instituição'}, () => this.devicesSlider.slickGoTo(2))
    }
    if (this.state.currentDevice === 'Instituição') {
      this.setState({currentDevice: 'Médico'}, () => this.devicesSlider.slickGoTo(1))
    }
    if (this.state.currentDevice === 'Médico') {
      this.setState({currentDevice: 'Paciente'}, () => this.devicesSlider.slickGoTo(0))
    }
  }

  render() {

    const content = this.props.data.allGoogleSheetSheet.edges[0].node

    return (

  <Layout headline={content.headline} lang='en'>

    <FeatureWrapper>
      <Feature title='1500' subtitle='Downloads' icon={calendarIcon} border={'1px solid #70ACDA'} />
      <Feature title='1500' subtitle='Distributed examinations' icon={plusIcon} border={'1px solid #70ACDA'}  />
      <Feature title='1500' subtitle='Schedules made' icon={calendarIcon} border={'1px solid #70ACDA'}  />
      <Feature title='1500' subtitle='Saved in the process' icon={calendarIcon} border='none'  />
    </FeatureWrapper>

    <Card className='patients'>
      
      
      <div style={{width: '100%'}}>
        <a id='pacientes' />
        <Slider {...sliderSettings}>

          <div>
            <h2>Patient</h2>
            <TwoColumnsWrapper style={{alignItems: 'flex-start'}}>
              <div style={{flexGrow: 1}}>
                <img alt='' style={{maxWidth: '100%'}} src={PatientImage} />
              </div>

              <div style={{flexGrow: 1, width: '100%', padding: '0 30px'}}>
                <div dangerouslySetInnerHTML={{ __html: content.patient }} />
                <LinkButton>Login</LinkButton>
              </div>
            </TwoColumnsWrapper>
          </div>

          <div>
            <h2>Doctor</h2>
            <TwoColumnsWrapper style={{alignItems: 'flex-start'}}>
              <div style={{flexGrow: 1, width: '100%', padding: '0 30px'}}>
                <div dangerouslySetInnerHTML={{ __html: content.doctor }} />
                <LinkButton>Login</LinkButton>
              </div>

              <div style={{flexGrow: 1}}>
                <img alt='' style={{maxWidth: '100%'}} src={DoctorImage} />
              </div>
            </TwoColumnsWrapper>
          </div>

          <div>
            <h2>Institution</h2>
            <TwoColumnsWrapper style={{alignItems: 'flex-start'}}>
              <div style={{flexGrow: 1}}>
                <img alt='' style={{maxWidth: '100%'}} src={PatientImage} />
              </div>

              <div style={{flexGrow: 1, width: '100%', padding: '0 30px'}}>
                <div dangerouslySetInnerHTML={{ __html: content.company }} />
                <LinkButton>Login</LinkButton>
              </div>
            </TwoColumnsWrapper>
          </div>

        </Slider>
      </div>

    </Card>

    <Card how>
      <a id='como-funciona' /><h2 style={{textAlign: 'center'}}>How It Works</h2>
      <h3 style={{textAlign: 'center', marginBottom: 60}}>{content.howitworks}</h3>

      <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', marginBottom: 30}}>
        <div style={{cursor: 'pointer'}} onClick={this.previousDevice}>{leftIcon}</div>
        <div style={{width: 140, textAlign: 'center', color: '#A5A5A5', fontSize: 20}}>{this.state.currentDevice}</div>
        <div style={{cursor: 'pointer'}} onClick={this.nextDevice}>{rightIcon}</div>
      </div>

      <Slider ref={ref => this.devicesSlider = ref}>

        <div>
          <Box>
            <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-evenly'}}>
              <DescriptionCard reverse icon={avatarIcon} title={"Texto Titulo"} subtitle={"loren ipsumm ipsummm..."}/>
              <DescriptionCard reverse icon={avatarIcon} title={"Texto Titulo"} subtitle={"loren ipsumm ipsummm..."}/>
              <DescriptionCard reverse icon={avatarIcon} title={"Texto Titulo"} subtitle={"loren ipsumm ipsummm..."}/>
            </div>

            <Device>
              <img alt='' style={{maxHeight: 400}} src={CelImage} />
            </Device>

            <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-evenly'}}>
              <DescriptionCard icon={avatarIcon} title={"Texto Titulo"} subtitle={"loren ipsumm ipsummm..."}/>
              <DescriptionCard icon={avatarIcon} title={"Texto Titulo"} subtitle={"loren ipsumm ipsummm..."}/>
              <DescriptionCard icon={avatarIcon} title={"Texto Titulo"} subtitle={"loren ipsumm ipsummm..."}/>
            </div>
          </Box>
        </div>

        <div>
          <Box>
            <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-evenly'}}>
              <DescriptionCard reverse icon={avatarIcon} title={"Texto Titulo"} subtitle={"loren ipsumm ipsummm..."}/>
              <DescriptionCard reverse icon={avatarIcon} title={"Texto Titulo"} subtitle={"loren ipsumm ipsummm..."}/>
              <DescriptionCard reverse icon={avatarIcon} title={"Texto Titulo"} subtitle={"loren ipsumm ipsummm..."}/>
            </div>

            <Device>
              <img alt='' style={{maxHeight: 400}} src={TabletImage} />
            </Device>

            <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-evenly'}}>
              <DescriptionCard icon={avatarIcon} title={"Texto Titulo"} subtitle={"loren ipsumm ipsummm..."}/>
              <DescriptionCard icon={avatarIcon} title={"Texto Titulo"} subtitle={"loren ipsumm ipsummm..."}/>
              <DescriptionCard icon={avatarIcon} title={"Texto Titulo"} subtitle={"loren ipsumm ipsummm..."}/>
            </div>
          </Box>
        </div>

        <div>
          <Box>
            <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-evenly'}}>
              <DescriptionCard reverse icon={avatarIcon} title={"Texto Titulo"} subtitle={"loren ipsumm ipsummm..."}/>
              <DescriptionCard reverse icon={avatarIcon} title={"Texto Titulo"} subtitle={"loren ipsumm ipsummm..."}/>
              <DescriptionCard reverse icon={avatarIcon} title={"Texto Titulo"} subtitle={"loren ipsumm ipsummm..."}/>
            </div>

            <Device>
              <img alt='' style={{maxHeight: 270}} src={NoteImage} />
            </Device>

            <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-evenly'}}>
              <DescriptionCard icon={avatarIcon} title={"Texto Titulo"} subtitle={"loren ipsumm ipsummm..."}/>
              <DescriptionCard icon={avatarIcon} title={"Texto Titulo"} subtitle={"loren ipsumm ipsummm..."}/>
              <DescriptionCard icon={avatarIcon} title={"Texto Titulo"} subtitle={"loren ipsumm ipsummm..."}/>
            </div>
          </Box>
        </div>

      </Slider>

    </Card>

    {/* <div>
    <a id='planos' /><h2 style={{textAlign: 'center'}}>Our Plans</h2>
      <h3 style={{textAlign: 'center', marginBottom: 40}}>{content.ourplans}</h3>

      <TwoColumnsWrapper>

        <div className='plan-card' style={{margin: 20, backgroundColor: 'white', fontSize: '13px', color: '#A5A5A5', padding: 20, maxWidth: 300}}>
          <h3 style={{fontWeight: 500, margin: '0  0 10px 0'}}>Plano Básico</h3>
          <span>Ao realizar o downgrade da sua conta:</span>
          <ul style={{lineHeight: 2, padding: 15}}>
            <li>Lorem ipsum dolor sit amet, consectetur</li>
            <li>Lorem ipsum dolor sit amet, consectetur</li>
            <li>Lorem ipsum dolor sit amet, consectetur</li>
          </ul>
          <div style={{display: 'flex', justifyContent: 'space-between'}}>
            <div>
              <span style={{color: '#70ACDA', fontWeight: 600, fontSize: '18px', lineHeight: '35px'}}>Free</span>
            </div>
            <div>
              <LinkButton style={{transform: 'scale(0.7)'}}>ESCOLHER</LinkButton>
            </div>
          </div>
        </div>

        <div className='plan-card' style={{margin: 20, backgroundColor: 'white', fontSize: '13px', color: '#A5A5A5', padding: 20, maxWidth: 300}}>
          <h3 style={{fontWeight: 500, margin: '0  0 10px 0'}}>Plano Básico</h3>
          <span>Ao realizar o downgrade da sua conta:</span>
          <ul style={{lineHeight: 2, padding: 15}}>
            <li>Lorem ipsum dolor sit amet, consectetur</li>
            <li>Lorem ipsum dolor sit amet, consectetur</li>
            <li>Lorem ipsum dolor sit amet, consectetur</li>
          </ul>
          <div style={{display: 'flex', justifyContent: 'space-between'}}>
            <div>
              <span style={{color: '#70ACDA', fontWeight: 600, fontSize: '18px', lineHeight: '35px'}}>Free</span>
            </div>
            <div>
              <LinkButton style={{transform: 'scale(0.7)'}}>ESCOLHER</LinkButton>
            </div>
          </div>
        </div>

      </TwoColumnsWrapper>
    </div> */}

    <Card who>
      <a id='quem-somos' /><h2 style={{textAlign: 'center'}}>Who We Are</h2>
      <h3 style={{textAlign: 'center', margin: '0 auto 40px auto', maxWidth: 600}}>{content.whoweare}</h3>

      <TwoColumnsWrapper>
        <div style={{margin: 30}}>
          <Avatar image={Andre}>
            <div className='bg' />
            <div className='caption'>
              <a href='http://lattes.cnpq.br/7434997626599082' target='_blank'>André Lucena</a><br />
              <a href='http://lattes.cnpq.br/7434997626599082' target='_blank'><strong style={{fontSize: 13}}>Lattes</strong></a>
            </div>
          </Avatar>
        </div>
        <div style={{margin: 30}}>
          <Avatar image={Antonio}>
            <div className='bg' />
            <div className='caption'><a href='https://www.linkedin.com/in/antonio-benchimol-mba-7bb4a7/' target='_blank'>Antônio Benchimol<br />{linkedinIcon}</a></div>
          </Avatar>
        </div>
        <div style={{margin: 30}}>
          <Avatar image={Bruno}>
            <div className='bg' />
            <div className='caption'><a href='https://www.linkedin.com/in/bruno-scott-6a129121/' target='_blank'>Bruno Scott<br />{linkedinIcon}</a></div>
          </Avatar>
        </div>
      </TwoColumnsWrapper>

    </Card>

    {/* <Card>
      <a id='depoimentos' /><h2 style={{textAlign: 'center'}}>Testimonials</h2>

      <div style={{display: 'flex', justifyContent: 'space-between'}}>
        <div style={{width: 40, height: 40, backgroundColor: 'hsla(206, 59%, 65%, 1)', borderRadius: '50%'}} onClick={this.previous}>
          <svg fill='white' viewBox="0 0 24 24">
            <path d='M 13 5.9296875 L 6.9296875 12 L 13 18.070312 L 14.5 16.570312 L 9.9296875 12 L 14.5 7.4296875 L 13 5.9296875 z'></path>
          </svg>
        </div>

        <div style={{width: 'calc(100% - 80px)'}}>
        <Slider {...sliderSettings} dots={false} ref={ref => this.slider = ref}>
          <div>
            <div style={{display: 'flex', justifyContent: 'center', flexDirection: 'row'}}>
              <div style={{width: 120}}>
                <div style={{margin: '30px 30px 20px 0', width: 120, height: 120, borderRadius: '50%', backgroundSize: 'cover', backgroundImage: `url(${PlaceholderImage})`}} />
                <div style={{color: 'hsla(0, 0%, 65%, 1)'}}><strong>Lorem ipsum</strong><br />Lorem ipsum</div>
              </div>
              <div style={{maxWidth: 200, padding: 30}}><h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</h3></div>
            </div>
          </div>
          <div>
            <div style={{display: 'flex', justifyContent: 'center', flexDirection: 'row'}}>
              <div style={{width: 120}}>
                <div style={{margin: '30px 30px 20px 0', width: 120, height: 120, borderRadius: '50%', backgroundSize: 'cover', backgroundImage: `url(${PlaceholderImage})`}} />
                <div style={{color: 'hsla(0, 0%, 65%, 1)'}}><strong>Lorem ipsum</strong><br />Lorem ipsum</div>
              </div>
              <div style={{maxWidth: 200, padding: 30}}><h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</h3></div>
            </div>
          </div>
        </Slider>
        </div>

        <div style={{width: 40, height: 40, backgroundColor: 'hsla(206, 59%, 65%, 1)', borderRadius: '50%'}} onClick={this.next}>
          <svg fill='white' viewBox="0 0 24 24">
            <path d="M 10 5.9296875 L 8.5 7.4296875 L 13.070312 12 L 8.5 16.570312 L 10 18.070312 L 16.070312 12 L 10 5.9296875 z"></path>
          </svg>
        </div>
      </div>

    </Card> */}

    {/* <Card clients>
      <a id='clientes' /><h2 style={{textAlign: 'center'}}>Clients</h2>
      <h3 style={{textAlign: 'center', marginBottom: 40}}>{content.clients}</h3>
      <div>
        <Slider {...sliderSettings}>
          <div>
            <img alt='' style={{maxWidth: '100%'}} src={ClientsImage} />
          </div>
          <div>
            <img alt='' style={{maxWidth: '100%'}} src={ClientsImage} />
          </div>
          <div>
            <img alt='' style={{maxWidth: '100%'}} src={ClientsImage} />
          </div>
        </Slider>
      </div>
    </Card> */}

    {/* <Card>
      <a id='blog' /><h2 style={{textAlign: 'center'}}>Blog</h2>
      <h3 style={{textAlign: 'center', marginBottom: 40}}>{content.blog}</h3>

      <Blog>

        <BlogPostBox>
          <div className='overlay' style={{position: 'relative'}}>
            <span className='headline'>Olá, mundo!</span>
            <a href='http://blog.docpadapp.com.br/2018/11/25/ola-mundo/' className='blogbtn' style={{cursor: 'pointer', position: 'absolute', bottom: 20, right: 20, backgroundColor: 'hsla(352, 60%, 50%, 1)', color: 'white', padding: '14px 20px', fontSize: 12, borderRadius: 20, paddingRight: 50}}>
              <span>LEIA MAIS</span>
              <div style={{display: 'block', backgroundColor: 'hsla(352, 60%, 41%, 1)', borderRadius: '50%', position: 'absolute', right: 6, top: 6, width: 30, height: 30}}>                
                <svg fill='white' viewBox='0 0 24 24'><path d='M 10 5.9296875 L 8.5 7.4296875 L 13.070312 12 L 8.5 16.570312 L 10 18.070312 L 16.070312 12 L 10 5.9296875 z' /></svg>
              </div>
            </a>
          </div>
        </BlogPostBox>

        <BlogPostBox>
          <div className='overlay' style={{position: 'relative'}}>
            <span className='headline'>Lorem ipsum dolor</span>
            <div className='blogbtn' style={{cursor: 'pointer', position: 'absolute', bottom: 20, right: 20, backgroundColor: 'hsla(352, 60%, 50%, 1)', color: 'white', padding: '14px 20px', fontSize: 12, borderRadius: 20, paddingRight: 50}}>
              <span>LEIA MAIS</span>
              <div style={{display: 'block', backgroundColor: 'hsla(352, 60%, 41%, 1)', borderRadius: '50%', position: 'absolute', right: 6, top: 6, width: 30, height: 30}}>                
                <svg fill='white' viewBox='0 0 24 24'><path d='M 10 5.9296875 L 8.5 7.4296875 L 13.070312 12 L 8.5 16.570312 L 10 18.070312 L 16.070312 12 L 10 5.9296875 z' /></svg>
              </div>
            </div>
          </div>
        </BlogPostBox>

        <BlogPostBox>
          <div className='overlay' style={{position: 'relative'}}>
            <span className='headline'>Lorem ipsum dolor</span>
            <div className='blogbtn' style={{cursor: 'pointer', position: 'absolute', bottom: 20, right: 20, backgroundColor: 'hsla(352, 60%, 50%, 1)', color: 'white', padding: '14px 20px', fontSize: 12, borderRadius: 20, paddingRight: 50}}>
              <span>LEIA MAIS</span>
              <div style={{display: 'block', backgroundColor: 'hsla(352, 60%, 41%, 1)', borderRadius: '50%', position: 'absolute', right: 6, top: 6, width: 30, height: 30}}>                
                <svg fill='white' viewBox='0 0 24 24'><path d='M 10 5.9296875 L 8.5 7.4296875 L 13.070312 12 L 8.5 16.570312 L 10 18.070312 L 16.070312 12 L 10 5.9296875 z' /></svg>
              </div>
            </div>
          </div>
        </BlogPostBox>

      </Blog>
    </Card> */}

    <Card>
      <a id='contato' />
      <TwoColumnsWrapper>
        <div style={{flexGrow: 1, width: '100%', height: 500}}>
          {/* <img alt='' src={MapImage} /> */}
          <iframe frameBorder='0' style={{border: 0, width: '100%', height: '100%'}} src='https://maps.google.com/maps?q=Rua%20Vice-Governador%20Rubens%20Berardo%2C%2065&t=&z=15&ie=UTF8&iwloc=&output=embed' allowFullScreen />
        </div>
        <div style={{flexGrow: 1, width: '100%', padding: '0 60px'}}>
          <h2>Contact</h2>
          <h3 style={{marginBottom: 40}}>Contact us</h3>
          <div><input type='text' placeholder='Full Name' /></div>
          <div><input type='email' placeholder='Email' /></div>
          <div><textarea placeholder='Message' /></div>
          <LinkButton>Enviar</LinkButton>

            <div style={{display: 'flex', flexDirection: 'row', marginTop: 80}}>
              <div style={{color: '#BE4252', paddingTop: 4, paddingRight: 10}}>{pinIcon}</div>
              <div><p style={{lineHeight: 1.6}}>R. Vice-Governador Rubens Berardo, 65<br />Gávea - Rio de Janeiro - RJ<br />CEP: 22451-070</p></div>
            </div>
          
        </div>
      </TwoColumnsWrapper>
    </Card>

  </Layout>
)
    }
  }

export const pageQuery = graphql`
  query {
    allGoogleSheetSheet {
      edges {
        node {
          headline
          whoweare
          blog
          howitworks
          patient
          doctor
          company
          ourplans
          clients
        }
      }
    }
  }
`

export default IndexPage
